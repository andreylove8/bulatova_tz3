from codehw2 import increment_data, my_sum, my_min, my_mult, my_max


def test_speed():
    from timeit import timeit

    increment_data('small_input.txt', 'big_input.txt')  # you can comment this line if you have already run it
    small_set_result = timeit("count_funcs('small_input.txt')", setup="from codehw2 import count_funcs", number=1)
    big_set_result = timeit("count_funcs('big_input.txt')", setup="from codehw2 import count_funcs", number=1)
    print("small dataset time:\t", small_set_result)
    print("big dataset time:\t", big_set_result)
    assert small_set_result < big_set_result


def test_my_sum():
    assert my_sum([1, 2, 3]) == 6
    assert my_sum([-1, 1, 0]) == 0


def test_my_mult():
    assert my_mult([1, 2, 3]) == 6
    assert my_mult([1, 0, 2]) == 0
    assert my_mult([2, -1, 5]) == -10


def test_my_min():
    assert my_min([1, 2, -1]) == -1
    assert my_min([1, 2, 3]) == 1
    assert my_min([0, 1, 9]) == 0


def test_my_max():
    assert my_max([1, 2, -1]) == 2
    assert my_max([1, 2, 3]) == 3
    assert my_max([0, 1, 9]) == 9


def except_error(function_n, *args, **kwargs):
    try:
        function_n(args, kwargs)
        return False
    except:
        return True


def test_empty_sets():
    assert my_sum([]) == 0  # we want 0 from sum if there was an empty data arr given
    assert my_mult([]) == 1  # we want 1 from mult if there was an empty data arr given
    assert except_error(my_min, [])
    assert except_error(my_max, [])


if __name__ == "__main__":
    test_speed()
    test_my_sum()
    test_my_mult()
    test_my_min()
    test_my_max()
    test_empty_sets()
