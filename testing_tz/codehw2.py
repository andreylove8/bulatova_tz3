def my_sum(data: list):
    curr_sum = 0
    for i in data:
        curr_sum += i
    return curr_sum


def my_mult(data: list):
    """
    returns multiply of all values in `data`,
    if `data` is empty returns 1,
    if any error occures returns -1 instead.

    in fact, in pure python we will not have integer overflow,
    because python integers have arbitrary precision;
    but if it happens, we catch it and return -1
    (that means that program won't crash due to overflow)
    """
    curr_multi = 1
    try:
        for i in data:
            curr_multi *= i
    except:
        return -1
    return curr_multi


def my_max(data: list):
    cur_max = data[0]
    for i in data:
        cur_max = max(cur_max, i)
    return cur_max


def my_min(data: list):
    cur_min = data[0]
    for i in data:
        cur_min = min(cur_min, i)
    return cur_min


def get_int_array(source_fn):
    data_arr = []
    with open(source_fn, 'r') as f:
        for line in f:
            for i in line.split():
                if i.isdigit():
                    data_arr.append(int(i))
                else:
                    continue
    return data_arr


def count_funcs(source_fn):
    data_arr = get_int_array(source_fn)
    ans_multi = my_mult(data_arr)
    ans_max = my_max(data_arr)
    ans_min = my_min(data_arr)
    ans_sum = my_sum(data_arr)
    return ans_max, ans_multi, ans_min, ans_sum


def increment_data(input_fn: str, output_fn: str, coef: int = 3):
    source_data = get_int_array(input_fn)
    counter = 0
    with open(output_fn, 'w') as file:
        while counter < coef:
            file.write(" ".join(map(str, source_data)))
            file.write(" ")
            counter += 1


def pretty_print(source_fn: str, results: tuple):
    print(f"run with `{source_fn}` file ")
    print("\n".join(map(lambda line: line[0] + ':\t' + str(line[1]), zip(["max", "mul", "min", "sum"], results))))
    print()


if __name__ == "__main__":
    increment_data("small_input.txt", "big_input.txt")
    pretty_print("small_input.txt", count_funcs("small_input.txt"))
    pretty_print("big_input.txt", count_funcs("big_input.txt"))
